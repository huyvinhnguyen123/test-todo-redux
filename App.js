import React from 'react';
import Index from './src/index';
import store from './src/store/indexStore';
import { Provider } from 'react-redux';

export default function App() {

  return (
  	<Provider store={store}>
    	<Index />
	</Provider>
  );
}

