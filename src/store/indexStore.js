import { createStore } from 'redux';
import rootRedux from '../reducers/todos';

const store = createStore(rootRedux);

export default store