import React from 'react';
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native'
import { useSelector, useDispatch } from 'react-redux';
import { deleteTodoAction } from "../actions/indexAction";

export default function TodoList (){
	
	const todoList= useSelector((state) => state.todos);
	const dispatch = useDispatch();

	return (
		<View>

			<FlatList
				data={todoList}
				keyExtractor = {item => item.key}
				renderItem={({ item }) => (
					<TouchableOpacity key={item.id} onPress={() => dispatch(deleteTodoAction(item.id))}>
							<View style={styles.itemTodo}> 
								<Text style={styles.text}>{item.text}</Text> 
							</View>
					</TouchableOpacity>
				)}
			/>

			{/* {
				todoList.map((todo) => {
					return (
						<TouchableOpacity key={todo.id} onPress={() => dispatch(deleteTodoAction(todo.id))}>
							<Text> {todo.text} </Text>
						</TouchableOpacity>
					)
				})
			} */}

		</View>
	);
}

const styles = StyleSheet.create({
	itemTodo: {
		backgroundColor: '#333',
		flexDirection: 'row',
		padding: 16,
		marginTop: 16,
		borderColor:'#bbb',
		borderWidth: 1,
		borderStyle: 'solid',
		borderRadius: 10
	},

	text: {
		color: "#fff",
  		textAlign: "center",
		fontSize: 17,
		marginLeft: 20
	}
})
