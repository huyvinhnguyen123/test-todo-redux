import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Header(){
	return (
		<View style={styles.head}>
			<Text style={styles.title}> Todo App </Text>
			<Text style={styles.title}> (Redux + Hook) </Text>
		</View>
	);
}

const styles = StyleSheet.create({
	head: {
		textAlign: 'center',
		padding: 30,
		backgroundColor: '#ffa836'
	},

	title: {
		fontSize: 20,
		fontWeight: 'bold',
		color: '#fff'
	}
})