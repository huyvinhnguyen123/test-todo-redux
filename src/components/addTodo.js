import React, { useState } from 'react';
import { StyleSheet, View, TextInput, Button } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { addTodoAction } from  '../actions/indexAction';

export default function AddTodo() {

    const [ todo, setTodo ] = useState('');

    const todos = useSelector((state) => state.todos);
    const dispatch = useDispatch();

    const inputTodo = (value) => {
        setTodo(value);
    }

    const addTodo = () => {
        const newTodo = { text: todo, id: todos.length};
        dispatch(addTodoAction(newTodo));
        setTodo("");
        console.log(newTodo);
    } 

    return (
        <View>
            <TextInput style={styles.input} placeholder="Type your todo" onChangeText={inputTodo} />
            <Button title="Add" onPress={() => addTodo()} />
        </View>
    );
}

const styles = StyleSheet.create({
    input:{
        color: '#000',
		marginBottom: 10,
        paddingHorizontal: 8,
        paddingVertical: 6,
        borderBottomWidth: 1,
        borderBottomColor: '#ffa836'
    }
})