import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import AddTodo from './components/addTodo';
import Header from './components/header';
import TodoList from './components/todoList';

export default function Index() {
  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.content}>
        <AddTodo />
        <View style={styles.list}>
          <TodoList />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },

  content: {
    paddingTop: 20
  },

  list: {
    paddingHorizontal: 16
  }
});
